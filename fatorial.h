/* Algoritimo desenvolvido na aula de LP1 (linguagem de programação)
/  Autor: Arthur Cohen
/  Curso: Bacharel em Tenologia da Informacao - UFRN
/  04/03/2017
*/

#ifndef FATORIAL_H
#define FATORIAL_H

//define a funcao que retorna o resultado do fatorial de um numero natural k>0
int fator(int k);

#endif
